package id.co.hanatekindo.login.Dao;

import id.co.hanatekindo.login.model.Authorities;
import id.co.hanatekindo.login.model.RedirectUrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AuthDaoImpl implements AuthDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public Authorities getUserAuthority(String username,String authorities) {
		Authorities authoritiesUser = null;
		try {
			String sql = "select A.*,client_id,client_password from authorities A inner join users B on B.username=A.username\n" +
					"inner join mst_oauth_redirect_url C on C.authority=A.authority\n" +
					"where B.nip=? and A.authority=?";
			authoritiesUser = jdbcTemplate.queryForObject(sql,
					new BeanPropertyRowMapper<Authorities>(Authorities.class),username,authorities);

		}catch (Exception E){
			E.printStackTrace();
		}
		return authoritiesUser;
	}

	@Override
	public List<RedirectUrl> getUrl() {
		List<RedirectUrl> list;
		String sql = "select * from mst_oauth_redirect_url";
		list= jdbcTemplate.query(sql, new BeanPropertyRowMapper<RedirectUrl>(RedirectUrl.class));
		return  list;
	}

	@Override
	public String getLayanan(String redirectUrl) {
		String result = "";
		String sql = "select authority from mst_oauth_redirect_url where redirect_url=?";
		result = jdbcTemplate.queryForObject(sql, String.class, redirectUrl);
		return result;
	}
}
