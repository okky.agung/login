package id.co.hanatekindo.login.Dao;
import id.co.hanatekindo.login.model.Authorities;
import id.co.hanatekindo.login.model.RedirectUrl;

import java.util.List;

public interface AuthDao {
	public Authorities getUserAuthority(String username, String authorities) ;

	List<RedirectUrl> getUrl();

    String getLayanan(String redirectUrl);
}
