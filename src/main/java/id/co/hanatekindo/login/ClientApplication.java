package id.co.hanatekindo.login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class ClientApplication {

	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(ClientApplication.class);
		application.setAdditionalProfiles("ssl");
		application.run(args);
	}

}
