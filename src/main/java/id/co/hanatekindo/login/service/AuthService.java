package id.co.hanatekindo.login.service;

import id.co.hanatekindo.login.Dao.AuthDao;
import id.co.hanatekindo.login.model.Authorities;
import id.co.hanatekindo.login.model.RedirectUrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service("authService")
public class AuthService {

	@Autowired
	AuthDao authDao;

	@Transactional
	public Authorities getAuth(String username,String authorities) {
		return authDao.getUserAuthority(username,authorities);
	}


	@Transactional
	public List<RedirectUrl> getRedirectUrl() {
		return authDao.getUrl();
	}

    public String getLayanan(String redirectUrl) {
		return authDao.getLayanan(redirectUrl);
    }
}
