package id.co.hanatekindo.login.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import id.co.hanatekindo.login.constant.Constant;
import id.co.hanatekindo.login.constant.HttpPostForm;
import id.co.hanatekindo.login.model.*;
import id.co.hanatekindo.login.model.CheckToken.ErrorResponse;
import id.co.hanatekindo.login.model.CheckToken.SuccessResponse;
import id.co.hanatekindo.login.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.InetAddress;
import java.util.*;

@Controller
@RequestMapping("/")
@EnableWebSecurity
public class MainController {

    @Autowired
    AuthService authService;

    @Autowired
    private UserDetailsService userDetailsService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(@RequestParam(required = false) String redirectUrl,
                            @RequestParam(required = false) String tag,
                            @RequestParam(required = false) String error,ModelMap modelMap) {

        List<RedirectUrl> redirectUrls = authService.getRedirectUrl();

        for(int i = 0; i < redirectUrls.size(); i++) {
            if(redirectUrls.get(i).getAuthority().equals("ESIDIK")){
                modelMap.addAttribute("urlesidik",redirectUrls.get(i).getRedirect_url());
            }
            if(redirectUrls.get(i).getAuthority().equals("ESIKMA")){
                modelMap.addAttribute("urlesikma",redirectUrls.get(i).getRedirect_url());
            }
            if(redirectUrls.get(i).getAuthority().equals("ESIMU")){
                modelMap.addAttribute("urlesimu",redirectUrls.get(i).getRedirect_url());
            }
            if(redirectUrls.get(i).getAuthority().equals("ESIMADA")){
                modelMap.addAttribute("urlesimada",redirectUrls.get(i).getRedirect_url());
            }
        }

        if(error != null){
            modelMap.addAttribute("Error",true);
            modelMap.addAttribute("ErrorMessage",error);
        }else{
            modelMap.addAttribute("Error",false);
        }

        if(redirectUrl != null){
            modelMap.addAttribute("default",true);
            modelMap.addAttribute("redirectUrl",redirectUrl);
            modelMap.addAttribute("layanan",tag.toLowerCase());
        }else{
            modelMap.addAttribute("default",false);
        }

         return "page-login-d1";
    }


    @RequestMapping(value="/loginProcess", method = RequestMethod.POST, headers = "Accept=application/json")
    public String getShipmentDetails(@RequestParam(value="username", defaultValue="0") String username,
                                     @RequestParam(value="password", defaultValue="0") String password,
                                     @RequestParam(value="from", defaultValue="null") String from,
                                     @RequestParam(value="default", defaultValue="null") boolean defaults,
                                     @RequestParam(value="url", defaultValue="null") String url,
                                     @RequestParam(value="groupUser", defaultValue="9") String groupUser,
                                     @RequestHeader(value = "User-Agent") String userAgent) throws IOException {

        try {
            Authorities auth = authService.getAuth(username,from);

            if(auth != null){
                if(auth.getUser_id()!=0){
                    Gson gson = new GsonBuilder().disableHtmlEscaping().create();
                    Map<String, String> headers = new HashMap<>();
                    headers.put("User-Agent", userAgent);
                    headers.put("Content-Length", String.valueOf(userAgent.length()));
                    headers.put("Host", InetAddress.getLocalHost().getHostName());
                    headers.put("Authorization", Constant.base64(auth.getClient_id(),auth.getClient_password()));

                    HttpPostForm httpPostForm = new HttpPostForm(HttpPostForm.URL+HttpPostForm.path_oauth, "utf-8", headers);
                    // Add form field
                    httpPostForm.addFormField("grant_type", "password");
                    httpPostForm.addFormField("username", username);
                    httpPostForm.addFormField("password", password);
                    // Result
                    String response = httpPostForm.finish();

                    System.out.println(response);

                    JsonResponse responseToken = new JsonResponse();
                    responseToken.setResponseToken(gson.fromJson(response,ResponseToken.class));

                    if(!responseToken.getResponseToken().getAccess_token().equals("null")){
                        String redirectUrl = url;
                        return "redirect:" + redirectUrl+"?access="+responseToken.getResponseToken().getAccess_token()+"&refresh="+responseToken.getResponseToken().getRefresh_token()+"&nip="+username+"&group="+groupUser+"&expiration="+responseToken.getResponseToken().getExpires_in();
                    }else{
                        if(defaults == true){
                            return "redirect:/login?tag="+from+"&redirectUrl="+url+"&error=Authentication Failed";
                        }else{
                            return "redirect:/login?tag="+from+"&error=Authentication Failed";
                        }
                    }
                }
            }else{
                if(defaults == true){
                    return "redirect:/login?tag="+from+"&redirectUrl="+url+"&error=Authentication Failed";
                }else{
                    return "redirect:/login?tag="+from+"&error=Authentication Failed";
                }

            }
        }catch (Exception E){
            if(defaults == true){
                return "redirect:/login?tag="+from+"&redirectUrl="+url+"&error=Please Contact Your Admin";
            }else{
                return "redirect:/login?tag="+from+"&error=Please Contact Your Admin";
            }
        }
        return "redirect:/login";
    }


    @RequestMapping(value = "/oauth/token")
    public
    @ResponseBody
    Object token(@RequestParam String grant_type,
                 @RequestParam String username,
                 @RequestParam String password,
                 @RequestHeader (name="Authorization") String token,
                 @RequestHeader(value = "User-Agent") String userAgent) {

        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        try {
            System.out.println("Request : grant_type :"+grant_type+" username :"+username+" password :"+password+" token :"+token+" userAgent :"+userAgent);
            String values [] = Constant.decode(token);

            String users = values[0];
            String passwords = values[1];

            System.out.println(users);
            System.out.println(passwords);

            if(!Constant.base64(users,password).equals("notmatch")){

                Map<String, String> headers = new HashMap<>();
                headers.put("User-Agent", userAgent);
                headers.put("Content-Length", String.valueOf(userAgent.length()));
                headers.put("Host", InetAddress.getLocalHost().getHostName());
                headers.put("Authorization", Constant.base64(users,passwords));

                HttpPostForm httpPostForm = new HttpPostForm(HttpPostForm.URL+HttpPostForm.path_oauth, "utf-8", headers);
                // Add form field
                httpPostForm.addFormField("grant_type", grant_type);
                httpPostForm.addFormField("username", username);
                httpPostForm.addFormField("password", password);
                // Result
                String response = httpPostForm.finish();
                JsonResponse responseToken = new JsonResponse();
                responseToken.setResponseToken(gson.fromJson(response,ResponseToken.class));


                if(responseToken.getResponseToken().getAccess_token() != null){
                    if(!responseToken.getResponseToken().getAccess_token().equals(null)){
                        OauthResponse response1 = new OauthResponse();
                        response1.setAccess_token(responseToken.getResponseToken().getAccess_token());
                        response1.setRefresh_token(responseToken.getResponseToken().getRefresh_token());
                        response1.setNip(username);
                        response1.setStatus("success");
                        response1.setExpired_in(responseToken.getResponseToken().getExpires_in());

                        System.out.println("Responses : "+ gson.toJson(response1));

                        return  response1;
                    }else{
                        ErrorOauth errorOauth = new ErrorOauth();
                        errorOauth.setError_message(responseToken.getResponseToken().getError_description());
                        errorOauth.setActive(responseToken.getResponseToken().getActive());
                        errorOauth.setExpired(responseToken.getResponseToken().getExpired());
                        errorOauth.setStatus("error");
                        System.out.println("Responses : "+ gson.toJson(errorOauth));
                        return  errorOauth;
                    }
                }else{
                    ErrorOauth errorOauth = new ErrorOauth();
                    errorOauth.setError_message(responseToken.getResponseToken().getError_description());
                    errorOauth.setActive(responseToken.getResponseToken().getActive());
                    errorOauth.setExpired(responseToken.getResponseToken().getExpired());
                    errorOauth.setStatus("error");
                    System.out.println("Responses : "+ gson.toJson(errorOauth));
                    return  errorOauth;
                }
            }else{
                ErrorOauth errorOauth = new ErrorOauth();
                errorOauth.setError_message("User Unauthorized");
                errorOauth.setActive(String.valueOf(false));
                errorOauth.setExpired("false");
                errorOauth.setStatus("error");
                System.out.println("Responses : "+ gson.toJson(errorOauth));

                return  errorOauth;
            }
        }catch (Exception E){
            E.printStackTrace();

            ErrorOauth errorOauth = new ErrorOauth();
            errorOauth.setError_message("Contact Your Administrator");
            errorOauth.setActive(String.valueOf(false));
            errorOauth.setExpired("false");
            errorOauth.setStatus("error");
            System.out.println("Responses : "+ gson.toJson(errorOauth));

            return  errorOauth;
        }
    }


    @RequestMapping(value = "/oauth/check_token")
    public
    @ResponseBody
    Object token(@RequestParam String token,
                 @RequestHeader (name="Authorization") String auth,
                 @RequestHeader(value = "User-Agent") String userAgent) {
        try {
            ErrorResponse errorResponse = new ErrorResponse();

            String values [] = Constant.decode(token);

            String users = values[0];
            String passwords = values[1];

            System.out.println(users);
            System.out.println(passwords);

            if(!Constant.base64(users,passwords).equals("notmatch")){
                Gson gson = new GsonBuilder().create();
                Map<String, String> headers = new HashMap<>();
                headers.put("User-Agent", userAgent);
                headers.put("Content-Length", String.valueOf(userAgent.length()));
                headers.put("Host", InetAddress.getLocalHost().getHostName());
                headers.put("Authorization", Constant.base64(users,passwords));

                HttpPostForm httpPostForm = new HttpPostForm(HttpPostForm.URL+HttpPostForm.path_check+"?token="+token, "utf-8", headers);
                String response = httpPostForm.finish();

                if(gson.fromJson(response, SuccessResponse.class).isActive() == true){
                    SuccessResponse response1 = gson.fromJson(response, SuccessResponse.class);
                    return response1;
                }else{
                    ErrorResponse response1 = gson.fromJson(response, ErrorResponse.class);
                    return response1;
                }
            }else{
                errorResponse.setActive(false);
                errorResponse.setExpired("false");
                errorResponse.setError("Authentication Failed");
                errorResponse.setError_description("Authentication Failed. Please Check Your Authorize");
                return errorResponse;
            }
        }catch (Exception E){
            E.printStackTrace();
        }

        return null;
    }


    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }

    @RequestMapping(value = "/access_denied", method = RequestMethod.GET)
    public String accessDeniedPage() {
        return "page-denied";
    }

    @RequestMapping
    String redirect() {
        return "redirect:/actuator/health";
    }
}
