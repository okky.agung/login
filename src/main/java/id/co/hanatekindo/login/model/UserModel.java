package id.co.hanatekindo.login.model;

import lombok.Data;

@Data
public class UserModel {
    String username;
    String password;
}
