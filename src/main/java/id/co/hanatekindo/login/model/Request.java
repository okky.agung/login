package id.co.hanatekindo.login.model;

import lombok.Data;

@Data
public class Request {
    String username;
    String grant_type;
    String password;
}
