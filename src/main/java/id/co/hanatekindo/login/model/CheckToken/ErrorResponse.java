package id.co.hanatekindo.login.model.CheckToken;

import lombok.Data;

@Data
public class ErrorResponse {
    String error;
    String error_description;
    String expired;
    boolean active;
}
