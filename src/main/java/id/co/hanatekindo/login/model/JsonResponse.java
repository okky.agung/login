package id.co.hanatekindo.login.model;

import lombok.Data;

@Data
public class JsonResponse {
    ResponseToken responseToken;
}

