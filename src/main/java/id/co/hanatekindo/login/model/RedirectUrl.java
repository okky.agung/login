package id.co.hanatekindo.login.model;

import lombok.Data;

@Data
public class RedirectUrl {
    String authority;
    String redirect_url;
}
