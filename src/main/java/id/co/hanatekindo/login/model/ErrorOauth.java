package id.co.hanatekindo.login.model;

import lombok.Data;

@Data
public class ErrorOauth {
    String status;
    String error_message;
    String active;
    String expired;

}
