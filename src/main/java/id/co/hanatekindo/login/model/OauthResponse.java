package id.co.hanatekindo.login.model;

import lombok.Data;

@Data
public class OauthResponse {
    String access_token;
    String refresh_token;
    String nip;
    String expired_in;
    String status;
}
