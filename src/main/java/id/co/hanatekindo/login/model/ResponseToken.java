package id.co.hanatekindo.login.model;

import lombok.Data;

@Data
public class ResponseToken {
    String access_token;
    String token_type;
    String refresh_token;
    String expires_in;
    String scope;
    String jti;
    String error;
    String error_description;
    String active;
    String expired;
}
