package id.co.hanatekindo.login.model.CheckToken;

import lombok.Data;

import java.util.ArrayList;

@Data
public class SuccessResponse {
    String user_name;
    boolean active;
    long exp;
    String client_id;
    String jti;
    ArrayList authorities;
    ArrayList scope;
}
