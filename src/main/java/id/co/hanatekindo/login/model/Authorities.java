package id.co.hanatekindo.login.model;

import lombok.Data;
@Data
public class Authorities {
    String username;
    String authority;
    int user_id;
    String client_id;
    String client_password;
}
